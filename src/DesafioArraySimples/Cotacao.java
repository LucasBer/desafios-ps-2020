package DesafioArraySimples;

public class Cotacao {
    private String titulo;

    public Cotacao(String titulo) {
        this.titulo = titulo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
}
