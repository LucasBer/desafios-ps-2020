package DesafioArraySimples;

import java.util.ArrayList;

public class CotacaoController {
    ArrayList<Cotacao> cotacoes = new ArrayList<>();

    public void addCotacao(Cotacao cot) {
        cotacoes.add(cot);
    }

    public Cotacao criarCotacao(String titulo){
        return new Cotacao(titulo);
    }

    public void excluirCotacao(String titulo) {
        for (int i = 0; i < cotacoes.size(); i++) {
            if (cotacoes.get(i).getTitulo().equals(titulo)) {
                cotacoes.remove(i);
            }
        }
    }

    public void alterarTitulo(String titulo,String novoTitulo){
        for (int i = 0; i < cotacoes.size(); i++) {
            if (cotacoes.get(i).getTitulo().equals(titulo)) {
                cotacoes.get(i).setTitulo(novoTitulo);
            }
        }
    }

    public void mostrarCotacoes(){
        for (int i = 0;i < cotacoes.size();i++){
            System.out.println(cotacoes.get(i).getTitulo());
        }
    }

}
