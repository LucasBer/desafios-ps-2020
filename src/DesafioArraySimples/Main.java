package DesafioArraySimples;

public class Main {
    public static void main(String[] args) {
        CotacaoController c = new CotacaoController();

        Cotacao dolar = c.criarCotacao("dolar");
        Cotacao bovespa = c.criarCotacao("bovespa");



        c.addCotacao(dolar);
        c.addCotacao(bovespa);
        c.alterarTitulo("dolar","real");
        c.excluirCotacao("bovespa");
        c.mostrarCotacoes();
    }
}
