package DesafioXadrez;

import java.util.ArrayList;
import java.util.Scanner;

public class ContagemPeca {
    ArrayList<Peca> pecas = new ArrayList<Peca>();
    //guardar as posições aonde se encontram as peças com seus valores
    int[] pos = new int[6];
    int cont = 0;

    Scanner sc = new Scanner(System.in);

    public void atribuirCodigos(){

        //instancia o vazio na primeira posicao do array
        pecas.add(new Peca("vazio",0));

        //cria os objetos de acordo com as entradas e depois coloca no array
        for(int i = 0;i < 6;i++){
            System.out.printf("escreva o nome da peça:");
            String nome = sc.next();
            /*codigo das pecas baseado na entrada fixa do Tabuleiro no codigo
            peao = 1
            bispo = 2
            cavalo = 3
            torre = 4
            rei = 5
            rainha = 6
             */
            System.out.printf("escreva o codigo dela(diferente de 0):");
            int cod = sc.nextInt();
            for(int j = 0;j <= cod;j++){
                pecas.add(new Peca("",0));
            }

            pecas.set(cod,new Peca(nome,cod));
            pos[cont] = cod;
            cont++;
        }

    }


    public void contarPecas(int[][] tabuleiro){
        for(int i = 0;i <= 7;i++){
            for(int j = 0;j <= 7;j++){

                pecas.get(tabuleiro[i][j]).quant++;
            }
        }
        System.out.println("----------------------------------------------------------------------------------------------------------------------");
        for(int i = 0;i < pos.length;i++){
            System.out.println(pecas.get(pos[i]).nome + ": " + pecas.get(pos[i]).quant);
        }
    }

}
