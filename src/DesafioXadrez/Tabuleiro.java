package DesafioXadrez;
public class Tabuleiro {
    int[][] tabuleiro = new int[8][8];
    //chama o iniciar tabuleiro na hora que a classe é instanciada
    Tabuleiro(){
        iniciarTabuleiro();
    }
    //tabuleiro feito conforme o exemplo
    public void iniciarTabuleiro(){
        tabuleiro[0][0] = 4;
        tabuleiro[0][1] = 3;
        tabuleiro[0][2] = 2;
        tabuleiro[0][3] = 5;
        tabuleiro[0][4] = 6;
        tabuleiro[0][5] = 2;
        tabuleiro[0][6] = 3;
        tabuleiro[0][7] = 4;

        tabuleiro[1][0] = 1;
        tabuleiro[1][1] = 1;
        tabuleiro[1][2] = 1;
        tabuleiro[1][3] = 1;
        tabuleiro[1][4] = 1;
        tabuleiro[1][5] = 1;
        tabuleiro[1][6] = 1;
        tabuleiro[1][7] = 1;

        tabuleiro[2][0] = 0;
        tabuleiro[2][1] = 0;
        tabuleiro[2][2] = 0;
        tabuleiro[2][3] = 0;
        tabuleiro[2][4] = 0;
        tabuleiro[2][5] = 0;
        tabuleiro[2][6] = 0;
        tabuleiro[2][7] = 0;

        tabuleiro[3][0] = 0;
        tabuleiro[3][1] = 0;
        tabuleiro[3][2] = 0;
        tabuleiro[3][3] = 0;
        tabuleiro[3][4] = 0;
        tabuleiro[3][5] = 0;
        tabuleiro[3][6] = 0;
        tabuleiro[3][7] = 0;

        tabuleiro[4][0] = 0;
        tabuleiro[4][1] = 0;
        tabuleiro[4][2] = 0;
        tabuleiro[4][3] = 0;
        tabuleiro[4][4] = 0;
        tabuleiro[4][5] = 0;
        tabuleiro[4][6] = 0;
        tabuleiro[4][7] = 0;

        tabuleiro[5][0] = 0;
        tabuleiro[5][1] = 0;
        tabuleiro[5][2] = 0;
        tabuleiro[5][3] = 0;
        tabuleiro[5][4] = 0;
        tabuleiro[5][5] = 0;
        tabuleiro[5][6] = 0;
        tabuleiro[5][7] = 0;

        tabuleiro[6][0] = 1;
        tabuleiro[6][1] = 1;
        tabuleiro[6][2] = 1;
        tabuleiro[6][3] = 1;
        tabuleiro[6][4] = 1;
        tabuleiro[6][5] = 1;
        tabuleiro[6][6] = 1;
        tabuleiro[6][7] = 1;

        tabuleiro[7][0] = 4;
        tabuleiro[7][1] = 3;
        tabuleiro[7][2] = 2;
        tabuleiro[7][3] = 5;
        tabuleiro[7][4] = 6;
        tabuleiro[7][5] = 2;
        tabuleiro[7][6] = 3;
        tabuleiro[7][7] = 4;


    }
}
