package EncontrarIndiceZero;

import java.util.ArrayList;

public class EncontrarIndice {
    int[] vetorBinario = {0,1,0,0,1,0,1,1,1,0,0,1};

    ArrayList<Sequencia> sequencias = new ArrayList<>();
    ArrayList<Posicao> posicoes = new ArrayList<>();

    //popula o array Sequencia com as posições de inicio ,fim e tamanho das sequencias de 1 separadamente
    public void popular(){
        for(int i = 0;i < vetorBinario.length;i++){
            int cont = 0;
            if(vetorBinario[i] == 1){
                cont++;

                for( int j = i;j < vetorBinario.length - 1;j++){
                    if(vetorBinario[j+1] == 1){
                        cont++;
                    }else{
                        break;
                    }
                }

                sequencias.add(new Sequencia(i,i+cont-1,cont));
                i+=cont-1;
            }
        }
    }

    //salva no array Posicao a chave e o tamanho de todas as combinações possiveis entre as sequencias de 1
    public void calcular(){
        for(int i = 0;i < sequencias.size();i++) {
            for (int j = 0; j < sequencias.size(); j++) {
                if (sequencias.get(i).fim == sequencias.get(j).inicio - 2) {
                    int tamanho = sequencias.get(j).fim - sequencias.get(i).inicio + 1;
                    int pos = sequencias.get(i).fim + 1;
                    posicoes.add(new Posicao(pos, tamanho));
                }
            }
        }
    }

    public void chave() {
        popular();
        calcular();
        int aux = posicoes.get(0).tamanho;
        int aux2 = 0;
        for (int i = 0; i < posicoes.size(); i++) {
            if(aux <= posicoes.get(i).tamanho){
                aux = posicoes.get(i).tamanho;
                aux2 = posicoes.get(i).chave;
            }
        }
        System.out.println(aux2 + 1);
    }

}
