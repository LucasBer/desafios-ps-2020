package EncontrarIndiceZero;

public class Sequencia {
    int inicio;
    int fim;
    int tamanho;

    public Sequencia(int inicio, int fim, int tamanho) {
        this.inicio = inicio;
        this.fim = fim;
        this.tamanho = tamanho;
    }

    @Override
    public String toString() {
        return "Sequencia{" +
                "inicio=" + inicio +
                ", fim=" + fim +
                ", tamanho=" + tamanho +
                '}';
    }
}
