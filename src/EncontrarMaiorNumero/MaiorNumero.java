package EncontrarMaiorNumero;

import java.util.ArrayList;
import java.util.Collections;

public class MaiorNumero {
    public void maiorNum(int a,int b){
        ArrayList<Integer> inteiros = new ArrayList<Integer>();
        inteiros.add(a);
        inteiros.add(b);
        Collections.sort(inteiros);

        System.out.println(inteiros.get(inteiros.size() - 1));
    }
}
