package MaiorProduto;

import java.util.ArrayList;

public class CalcularProduto {
    ArrayList<Integer> inteiros = new ArrayList<Integer>();

    public void produto(ArrayList<Integer> inteiros) {
        ArrayList<Integer> it = inteiros;
        //remove todos os zeros do vetor
        int tamanho = it.size();
        for (int i = 0; i < tamanho; i++) {
            if(it.get(i) == 0){
                it.remove(i);
                i--;
                tamanho--;
            }
        }

        //caso o vetor so tenha zeros
        if (it.size() == 0) {
            System.out.println("o vetor so possuia zeros, logo o produto é 0");
        } else {
            int cont = it.get(0);
            for (int i = 1; i < it.size(); i++) {
                cont *= it.get(i);
            }
            //verifica se possui algum numero negativo no array
            if (cont > 0) {
                //se todos forem positivos é so multiplicar tudo
                cont = it.get(0);
                System.out.print(cont);
                for (int i = 1; i < it.size(); i++) {
                    cont *= it.get(i);
                    System.out.print("," + it.get(i));
                }
                System.out.println();
                System.out.println("produto: " + cont);

            } else if (cont < 0) {//entra nessa if se tiver algum numero negativo
                int quantNegativo = 0;
                //conta quantos negativos existem no array
                for (int i = 0; i < it.size(); i++) {
                    if (it.get(i) < 0) {
                        quantNegativo++;
                    }
                }
                //se o numero de negativos for par,basta apenar multiplicar tudo
                if (quantNegativo % 2 == 0) {
                    System.out.print(cont);
                    for (int i = 1; i < it.size(); i++) {
                        cont *= it.get(i);
                        System.out.print("," + it.get(i));
                    }
                    System.out.println();
                    System.out.println("produto: " + cont);
                } else if (quantNegativo % 2 != 0) {
                    //se o numero de nativos for impar, basta remover o maior dos negativos
                    int aux = it.get(0);
                    boolean achou = false;
                    //procura qual o menor negativo
                    for (int i = 0; i < it.size(); i++) {

                        if (achou == false) {
                            if (it.get(i) < 0) {
                                aux = it.get(i);
                                achou = true;
                            }
                        } else {
                            if (it.get(i) < 0) {
                                if (aux < it.get(i)) {
                                    aux = it.get(i);
                                }
                            }

                        }

                    }
                    //remove o maior negativo

                    it.remove(it.indexOf(aux));

                    //multiplica tudo
                    cont = it.get(0);
                    System.out.print(cont);
                    for (int i = 1; i < it.size(); i++) {
                        cont *= it.get(i);
                        System.out.print("," + it.get(i));
                    }
                    System.out.println();
                    System.out.println("produto: " + cont);


                }

            }
        }
    }

}
