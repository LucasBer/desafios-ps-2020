package RobsonConstrucoes;

import java.util.ArrayList;

public class CargoCadastro {
    ArrayList<Cargo> cargos = new ArrayList<Cargo>();

    void addCargo(Double salario) {
        cargos.add(new Cargo(salario));
    }

    double valorSalario(int cod, ArrayList<Cargo> c) {
        return c.get(cod).getSalario();
    }
}
