package RobsonConstrucoes;

public class Funcionario {
    private int cod;
    private int cod_cargo;
    private String nome;

    public Funcionario(int cod, int cod_cargo, String nome) {
        this.cod = cod;
        this.cod_cargo = cod_cargo;
        this.nome = nome;
    }

    public int getCod() {
        return cod;
    }


    public int getCod_cargo() {
        return cod_cargo;
    }


    public String getNome() {
        return nome;
    }


}
