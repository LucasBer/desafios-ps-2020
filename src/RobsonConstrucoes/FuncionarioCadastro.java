package RobsonConstrucoes;

import java.util.ArrayList;

public class FuncionarioCadastro {
    ArrayList<Funcionario> funcionarios = new ArrayList<Funcionario>();

    public void addFuncionario(int cod, int cod_cargo, String nome,ArrayList<Cargo> c){
        boolean achouCod = false;
        boolean achouCargo = false;

        for (int i = 0;i < funcionarios.size();i++){
            if(cod == funcionarios.get(i).getCod()){
                achouCod = true;
                break;
            }
        }


            if(cod_cargo < c.size()){
                achouCargo = true;
            }


        if(!achouCargo){
            System.out.println("este cargo nao existe");
        }
        if(achouCod){
            System.out.println("ja existe um funcionario com esse codigo");
        }

        if(achouCargo && !achouCod) {
            funcionarios.add(new Funcionario(cod, cod_cargo, nome));
            System.out.println("funcionario " + nome + " adicionado");
        }
    }
}
