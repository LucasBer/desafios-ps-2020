package RobsonConstrucoes;

import java.util.ArrayList;

public class Relatorio {
    FuncionarioCadastro fc = new FuncionarioCadastro();
    CargoCadastro cc = new CargoCadastro();

    public void relatorioFunc(ArrayList<Funcionario> f, ArrayList<Cargo> c) {
        for (int i = 0; i < f.size(); i++) {
            System.out.println("cod: " + f.get(i).getCod());
            System.out.println("nome: " + f.get(i).getNome());
            System.out.println("salario: " + cc.valorSalario(f.get(i).getCod_cargo(), c));
            System.out.println("----------------------------------------------------------");
        }
    }

    public void salarioTotal(ArrayList<Funcionario> f, ArrayList<Cargo> c, int cod_cargo) {
        if (cod_cargo < c.size()) {
            double cont = 0;
            for (int i = 0; i < f.size(); i++) {
                if (cod_cargo == f.get(i).getCod_cargo())
                    cont += cc.valorSalario(f.get(i).getCod_cargo(), c);
            }
            System.out.println("valor total pago de salário aos funcionários que pertençam ao cargo " + cod_cargo + ": " + cont);
        } else {
            System.out.println("cargo inexistente");
        }
    }


}
